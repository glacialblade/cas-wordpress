<?php namespace App\PostTypes;

class SocialMediaPostType extends \App\PostTypes\Core\PostType {

	protected $metabox = [
        'Social Media Fields' => [
            'id'         => 'social_media_fields',
            'title'      => 'Social Media Fields',
            'pages'      => ['social-media'],
            'show_names' => true,
            'fields'     => [
                [ 'name' => 'URL',   'id' => 'url',   'type' => 'text' ],
                [ 'name' => 'Fontwesome Icon', 'description' => 'Please refer here: http://fontawesome.io/icons/',  'id' => 'icon',   'type' => 'text' ],
            ]
        ]
    ];

	// If the post type you want to control is page you don't need to specify these Attributes Anymore.

    // Post Type Name
    protected $label    = 'Social Media';
    protected $type     = 'social-media';

    // Default Not Required.
    protected $icon     = 'dashicons-facebook';
    protected $supports = ['title'];
    
    // Enqueue scripts here.
    public function scripts() {

    }

    // Custom Logic Here
    public function custom() {

    }

}