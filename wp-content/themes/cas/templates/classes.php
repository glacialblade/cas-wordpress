<?php
/*
Template Name: Classes
*/

get_header(); ?>
	
	<main>
		<div class="pagetitle" style="background: url(<?php the_post_thumbnail_url( 'full' ) ?>) center center no-repeat;">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="inner-content brushbg1">
			<div class="container-fluid padLR140">
				<div class="row">

					<div class="classes-wrapper">
						
					</div>
					
				</div>
			</div>
		</div>
		</div>
	</main>
<?php get_footer(); ?>

