<?php namespace App\PostTypes;

class TeacherPostType extends \App\PostTypes\Core\PostType {

	// If the post type you want to control is page you don't need to specify these Attributes Anymore.

	// Post Type Name
	protected $label    = 'Teacher'; 
	protected $type     = 'teacher';

	// Default Not Required.
	protected $icon     = 'dashicons-welcome-learn-more';
	protected $taxonomy = false;
	protected $exclude  = false;
	protected $supports = ['title', 'editor', 'thumbnail'];

	protected $metabox = [
        'Teacher Fields' => [
            'id'         => 'teacher',
            'title'      => 'Teacher Fields',
            'pages'      => ['teacher'],
            'show_names' => true,
            'fields'     => [
                [ 'name' => 'Position', 'id' => 'position', 'type' => 'text' ]
            ]
        ]
    ];
	
	// Enqueue scripts here.
	public function scripts() {

	}

	// Custom Logic Here
	public function custom() {

	}

}