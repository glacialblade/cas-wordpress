$(document).ready(function() {

	var BookingForm = {
		data: {},
		classCategories: [],

		el: {
			classForm:   $('form[name=class]'),
			loginForm:   $('form[name=login]'),
			resetForm:   $('form[name=reset-password]'),
			bookingForm: $('form[name=booking]'),
			changeTab:   $('.change-tab'),
			wizard:      $('.booking-wizard-menu'),
			tab: {
				class:    $('#tab-class'),
				login:    $('#tab-login'),
				booking:  $('#tab-booking'),
				thankyou: $('#tab-thank-you'),
			}
		},

		INIT: function() {
			this.PLUGINS();
			this.EVENTS();
			this.REQUESTS.getClassCategories($('[name=default_class]').val());
		},

		METHODS: {
			showTab: function(tab) {
				BookingForm.el.loginForm.show();
				BookingForm.el.resetForm.hide();
				if(tab != 'booking') {
					this.resetForm();
				}

				BookingForm.el.wizard.find('li').removeClass('current');
				BookingForm.el.tab.class.hide();
				BookingForm.el.tab.login.hide();
				BookingForm.el.tab.booking.hide();
				BookingForm.el.tab.thankyou.hide();

				BookingForm.el.wizard.find('#booking-wizard-item-'+tab).addClass('current');
				BookingForm.el.tab[tab].show();
			},

			resetForm: function() {
				var bookingForm = BookingForm.el.bookingForm;
				var loginForm   = BookingForm.el.loginForm;

				$('.password-note').hide();

				bookingForm.find('.error').removeClass('error');
				bookingForm.find('label.error, .help-block').remove();
				bookingForm.find('input, hidden, textarea, select').not('[name=type]').not('.multiselect-container input[type=checkbox]').val('');
				loginForm.find('input, button').val('').removeAttr('disabled');
			},

			findClassCategory: function(id) {
				for(var i in BookingForm.classCategories) {
					if(BookingForm.classCategories[i].id == id) {
						return BookingForm.classCategories[i];
					}
				}
			},

			findClass: function(id) {
				for(var i in BookingForm.classCategories) {
					for(var x in BookingForm.classCategories[i].classes) {
						if(BookingForm.classCategories[i].classes[x].id == id) {
							return BookingForm.classCategories[i].classes[x];
						}
					}
				}
			},

			populateBookingForm: function(data) {
				var form = BookingForm.el.bookingForm;
				$('.password-note').show();

				// Generic Fields
				data.email      = data.account_information.email;
				data.student_id = data.id;
				$.each(data, function(field, value) {
					form.find('[data-name="$name"]'.replace('$name', field)).val(value);
				});

				if(data.languages_spoken) {
					var languagesSpoken = data.languages_spoken.split(',');
					$.each(languagesSpoken, function(key, value) {
						if(value) {
							form.find('[name=languages_spoken]').find('option[value=$value]'.replace('$value', value)).attr('selected', 'selected');
						}
					});
					form.find('[name=languages_spoken]').multiselect('refresh');
				}

				// Contacts
				$.each(data.contact_information, function(field, value) {
					form.find('[data-name="$name"]'.replace('$name', field)).val(value);
				});

				// Work Backgrounds
				$.each(data.work_backgrounds, function(key, workBackground) {
					$.each(workBackground, function(field, value) {
						$('.work-background').eq(key).find('[data-name="$name"]'.replace('$name', field)).val(value);
					});
				});

				// Family Members
				$.each(data.family_members, function(key, familyMember) {
					$.each(familyMember, function(field, value) {
						$('.socio-economic-data').eq(key).find('[data-name="$name"]'.replace('$name', field)).val(value);
					});
				});
			}
		},

		PLUGINS: function() {
			/**
			 * Validations
			 */
			this.el.classForm.validate({ rules: { class_category_id: 'required', class_id: 'required', schedule: 'required' } });
			this.el.loginForm.validate({ rules: { email: 'required', password: 'required' } });
			this.el.resetForm.validate({ rules: { email: 'required' } });
			this.el.bookingForm.validate({ 
    			ignore: [],
				rules: { 
					email: { required: true, email: true }, password : { required: function() { return !BookingForm.el.bookingForm.find('[data-name=student_id]').val() ? true : false }, minlength : 5, equalTo : "[name=confirm_password]" },
					last_name: 'required', first_name: 'required', middle_name: 'required', birth_date: 'required', gender: 'required', citizenship: 'required', languages_spoken: 'required', educational_background: 'required', classification: 'required',
					employment_status: 'required', classification_of_work: 'required',
					street_number: 'required', barangay: 'required', city_town_province: 'required', postal_code: 'required', office_number: 'required', home_number: 'required', mobile_number: 'required',
					socio_name_0: 'required', socio_citizenship_0: 'required', socio_status_0: 'required', socio_educational_attainment_0: 'required', socio_occupation_0: 'required',
					socio_name_1: 'required', socio_citizenship_1: 'required', socio_status_1: 'required', socio_educational_attainment_1: 'required', socio_occupation_1: 'required',
				},
				errorPlacement: function(error, element) {
			        // Add the `help-block` class to the error element
			        error.addClass("help-block");

			        if (element.hasClass('multiselect')) {
			        	element.next('.btn-group').find('.multiselect').addClass('error');
			            error.insertAfter(element.next('.btn-group'))
			        } else {
						error.insertAfter(element);
					}
				}
			});
		},

		EVENTS: function() {
			var _this     = this;
			var classForm = this.el.classForm;

			$('#reset-password').click(function() {
				BookingForm.el.loginForm.hide();
				BookingForm.el.resetForm.show();
			});

			/**
			 * Change Tab Button
			 */
			this.el.changeTab.click(function() {
				_this.METHODS.showTab($(this).attr('data-show'));
			});

			/**
			 * ON CLASS FORM SUBMIT
			 */
			this.el.classForm.submit(function(e) {
				e.preventDefault();
				if($(this).valid()) {
					_this.data.classes = GLOBAL.simpleDataParser($(this));
					_this.METHODS.showTab('login');
				}
			});

			/**
			 * ON LOGIN FORM SUBMIT
			 */
			this.el.loginForm.submit(function(e) {
				var form = $(this);
				e.preventDefault();

				if(form.valid()) {
					BookingForm.REQUESTS.getStudentDetails();
				}
			});

			/**
			 * ON RESET FORM SUBMIT
			 */
			this.el.resetForm.submit(function(e) {
				var form = $(this);
				e.preventDefault();

				if(form.valid()) {
					BookingForm.REQUESTS.resetStudentPassword();
				}
			});

			/**
			 * ON BOOKING FORM SUBMIT
			 */
			this.el.bookingForm.submit(function(e) {
				e.preventDefault();

				if($(this).valid()) {
					$.each(['account-information', 'personal-information', 'employment-status', 'work-background', 'socio-economic-data', 'contact-information'], function(key, val) {
						_this.data[val] = GLOBAL.simpleDataParser($('.'+val));
					});
					_this.REQUESTS.submit();
				}
			});

			/**
			 * On Change Category
			 */
			classForm.find("[name=class_category_id]").change(function() {
				classForm.find('[name=class_id]').val('').attr('disabled', 'disabled');
				classForm.find('[name=schedule]').val('').attr('disabled', 'disabled');

				var classCategory = _this.METHODS.findClassCategory($(this).val());
				var options = '<option value="">-- Select a Class --</option>';
				classForm.find('[name=class_id]').html(options);

				if(classCategory) {
					$.each(classCategory.classes, function(key, val) {
						options += '<option value="$id">$label</option>'.replace('$id', val.id).replace('$label', val.name);
					});

					classForm.find('[name=class_id]').removeAttr('disabled');
					classForm.find('[name=class_id]').html(options);
				}

			});

			/**
			 * On Change Class
			 */
			classForm.find("[name=class_id]").change(function() {
				classForm.find('[name=schedule]').val('').attr('disabled', 'disabled');

				var _class = _this.METHODS.findClass($(this).val());
				var options = '<option value="">-- Choose your Schedule --</option>';
				classForm.find('[name=schedule]').html(options);

				if(_class.schedules) {
					var schedules = _class.schedules.split('\n');
					$.each(schedules, function(key, val) {
						options += '<option value="$label">$label</option>'.replace(/\$label/g, val)
					});

					classForm.find('[name=schedule]').removeAttr('disabled');
					classForm.find('[name=schedule]').html(options);
				}

			});
		},

		REQUESTS: {
			submit: function() {
				var form        = BookingForm.el.bookingForm;
				var data        = BookingForm.data;
				data.action     = 'save-booking';

				form.find('input, select, button').attr('disabled', 'disabled');
				$.ajax({
					url: GLOBAL.api.url+'/bookings/store',
					headers: { 'Authorization': GLOBAL.api.key },
					data: data,
					type: 'POST',
					success: function(data) {
						BookingForm.METHODS.showTab('thankyou');
					},
					error: function(data) {
						form.find('input, select, button').removeAttr('disabled');
						form.find('.alert').html(data.responseJSON.message).fadeIn();
						setTimeout(function() {
							form.find('.alert').fadeOut();
						}, 2500);

						$('html, body').animate({
					        scrollTop: form.offset().top
					    }, 500);
					}
				});
			},

			resetStudentPassword: function() {
				var form = BookingForm.el.resetForm;
				var data = form.serializeObject();

				form.find('input, button').attr('disabled', 'disabled');
				$.ajax({
					url: GLOBAL.api.url+'/users/reset-student-password',
					headers: { 'Authorization': GLOBAL.api.key },
					data: data,
					type: 'POST',
					success: function(data) {
						form.find('input, button').removeAttr('disabled');
						form.find('input').val('');
						form.find('.alert-success').fadeIn();
						setTimeout(function() {
							form.find('.alert-success').fadeOut();
						}, 2500);
					},
					error: function(data) {
						form.find('input, button').removeAttr('disabled');
						form.find('.alert-danger').fadeIn();
						setTimeout(function() {
							form.find('.alert-danger').fadeOut();
						}, 2500);
					}
				});
			},

			getStudentDetails: function() {
				var form = BookingForm.el.loginForm;
				var data = form.serializeObject();
				data.classes = BookingForm.data.classes;

				form.find('input, button').attr('disabled', 'disabled');
				$.ajax({
					url: GLOBAL.api.url+'/bookings/use-account-details',
					headers: { 'Authorization': GLOBAL.api.key },
					data: data,
					type: 'POST',
					success: function(data) {
						BookingForm.METHODS.populateBookingForm(data);
						BookingForm.METHODS.showTab('booking');
					},
					error: function(data) {
						form.find('input, button').removeAttr('disabled');
						form.find('.alert').fadeIn();
						setTimeout(function() {
							form.find('.alert').fadeOut();
						}, 2500);
					}
				});
			},

			getClassCategories: function(defaultValue) {
				$.ajax({
					url: GLOBAL.api.url+'/class-categories/get?with_schedule=true',
					type: 'GET',
					headers: { 'Authorization': GLOBAL.api.key },
					success: function(data) {
						BookingForm.classCategories = data;

						var form    = BookingForm.el.classForm;
						var options = '<option value="">-- Select a Class Category --</option>';

						$.each(data, function(key, val) {
							if(val.classes.length > 0) {
								options += '<option value="$id">$label</option>'.replace('$id', val.id).replace('$label', val.name);
							}
						});

						form.find('[name=class_category_id]').html(options);
						form.find('[name=class_category_id]').removeAttr('disabled');

						if(defaultValue && defaultValue != 0) {
							var _class = BookingForm.METHODS.findClass(defaultValue);
							form.find('[name=class_category_id]').val(_class.class_category_id).change();
							setTimeout(function() {
								form.find('[name=class_id]').val(_class.id).change();
							}, 500);
						}
					}
				});
			}
		}
	};

	BookingForm.INIT();
});