<p><strong>Employment Status</strong></p>
<div class="row employment-status" style="margin-top: -20px;">
	<div class="col-md-6 marT20">
		<select name="employment_status" data-name="employment_status" class="input input-small">
			<option value="">-- Employment Status --</option>
			<option value="Full Time">Full Time</option>
			<option value="Part Time">Part Time</option>
			<option value="Contractual">Contractual</option>
			<option value="Unemployed">Unemployed</option>
		</select>
	</div>
	<div class="col-md-6 marT20">
		<select name="classification_of_work" data-name="classification_of_work" class="input input-small">
			<option value="">-- Classification of Work --</option>
			<option value="Test">test</option>
		</select>
	</div>
</div>