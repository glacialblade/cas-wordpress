<?php get_header(); ?>
	<main>
		<div class="pagetitle" style="background: url(<?php the_post_thumbnail_url( 'full' ) ?>) center center no-repeat;">
			<h1>TEACHERS</h1>
		</div>
		<div class="inner-content brushbg1">
			<div class="container-fluid padLR140">
				<div class="row">
					<div class="teacher-image pull-left marR40">
						<img class="img-responsive" src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" width="450px" alt="">
					</div>
					<h3 class="fontS30"><?= get_the_title(); ?></h3>
					<p class="fontS25"><strong><?= get_post_meta(get_the_ID(), 'position', true); ?></strong></p>
					
					<?php while(have_posts()): the_post(); ?>
						<?= the_content(); ?>
					<?php endwhile; ?>

					<p class="text-center"><a class="btn btn-black" href="/teachers">BACK TO TEACHERS PAGE</a></p>
				</div>
			</div>
		</div>
		</div>
	</main>
<?php get_footer(); ?>