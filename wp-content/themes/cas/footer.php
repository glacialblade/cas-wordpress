	<footer>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-8">
					<div class="footer-nav">
						<?php wp_nav_menu([
							'theme_location' => 'footer',
							'menu_class' => ''
						]); ?>
					</div>
				</div>
				<div class="col-md-4 text-right">
					<div class="social-icons">
						<ul>
							<?php $socialMedias = new WP_Query([ 'post_type' => 'social-media', 'post_status' => 'publish', 'posts_per_page' => 5, 'order_by' => 'date', 'order' => 'DESC']); ?>
							<?php while($socialMedias->have_posts()): $socialMedias->the_post(); ?>
								<li><a target="_blank" href="<?= get_post_meta(get_the_ID(), 'url', true); ?>"><span class="fa <?= get_post_meta(get_the_ID(), 'icon', true); ?>" aria-hidden="true"></span></a></li>
							<?php endwhile; wp_reset_postdata(); ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="row copyright">
				<div class="text-center">
					<p><a href="#"><img class="img-responsive" src="<?= get_template_directory_uri(); ?>/assets/images/footer-logo.png" alt=""></a></p>
					<p class="fontS16">Copyright <?= date('Y'); ?></p>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
	<script src="<?= get_template_directory_uri(); ?>/assets/bower_components/jquery/dist/jquery.js"></script>
	<script src="<?= get_template_directory_uri(); ?>/assets/bower_components/bootstrap/dist/js/bootstrap.js"></script>
	<script src="<?= get_template_directory_uri(); ?>/assets/bower_components/jquery-validation/dist/jquery.validate.js"></script>
	<script src="<?= get_template_directory_uri(); ?>/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
	<script src="<?= get_template_directory_uri(); ?>/assets/bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
	<script src="<?= get_template_directory_uri(); ?>/assets/bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect-collapsible-groups.js"></script>
	<script src="<?= get_template_directory_uri(); ?>/assets/js/plugins/serialize-object.js"></script>

	<script src="<?= get_template_directory_uri(); ?>/assets/js/global.js"></script>
	<script type="text/javascript" src="<?= get_template_directory_uri(); ?>/assets/js/contact-form.js"></script>
	<script type="text/javascript" src="<?= get_template_directory_uri(); ?>/assets/js/booking-form.js"></script>
	<script type="text/javascript" src="<?= get_template_directory_uri(); ?>/assets/js/classes.js"></script>
</body>
</html>