<?php get_header(); ?>
	
	<main>
		<div class="pagetitle" style="background: url(<?php the_post_thumbnail_url( 'full' ) ?>) center center no-repeat;">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="inner-content brushbg1">
			<div class="container-fluid padLR140">
				<?php while(have_posts()): the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			</div>
		</div>
		</div>
		<?php require_once('parts/widgets/gallery.php'); ?>
		<?php require_once('parts/widgets/newsletter.php'); ?>
	</main>
<?php get_footer(); ?>