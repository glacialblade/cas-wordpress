<div class="reset-password">
	<p>
		<strong>Reset Password</strong>
	</p>
	<div class="alert alert-success" style="display: none;">
		Your new password has been successfully sent to your email.
	</div>
	<div class="alert alert-danger" style="display: none;">
		Invalid email entered.
	</div>
	<div class="row" style="margin-top: -20px">
		<div class="col-md-12 marT20">
			<input type="text" name="email" data-name="email" class="input input-small" placeholder="Email Address">
		</div>
		<div class="col-md-12 marT20">
			<button type="button" class="btn btn-small btn-black change-tab" data-show="login" class="btn btn-small btn-black">Back</button>&nbsp;
			<button type="submit" class="btn btn-small btn-black">Reset Password</button>&nbsp;
		</div>
	</div>
</div>