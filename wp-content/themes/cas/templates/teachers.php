<?php
/*
Template Name: Teachers
*/

get_header(); ?>
	
	<main>
		<div class="pagetitle" style="background: url(<?php the_post_thumbnail_url( 'full' ) ?>) center center no-repeat;">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="inner-content brushbg1">
			<div class="container-fluid padLR140">
				<div class="row">
					<?php
						$teachers =  new WP_Query([ 'post_type' => 'teacher', 'post_status' => 'publish', 'posts_per_page' => -1, 'order_by' => 'date', 'order' => 'ASC' ]);
					?>
					
					<?php while($teachers->have_posts()): $teachers->the_post(); ?>
						<div class="col-sm-4">
							<div class="teacher" style="position: relative;">
								<img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" />
								<div class="teacher-overlay">
									<div class="teacher-overlay-content-wrapper">
										<div class="teacher-overlay-content">
											<h3 class="fontS30"><?= get_the_title(); ?> </h3>
											<p class="fontS25"><?= get_post_meta(get_the_ID(), 'position', true); ?></p>
											<p><a class="btn btn-white" href="<?= get_the_permalink(); ?>">CLICK FOR MORE INFO</a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; wp_reset_postdata(); ?>

				</div>
			</div>
		</div>
		</div>
	</main>
<?php get_footer(); ?>