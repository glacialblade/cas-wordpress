$(document).ready(function() {

	var Classes = {
		data: {},

		el: {

		},

		INIT: function() {
			this.PLUGINS();
			this.EVENTS();
			this.REQUESTS.getClassCategories($('[name=default_class]').val());
		},

		METHODS: {
			fixDisplay: function(classCategories) {
				var html = '';
				$.each(classCategories, function(key, classCategory) {
					html += '\
					    <h2 class="fontS50 text-center">$name</h2>\
				 	    <div id="accordion-parent-$id" class="accordion panel-group">\
				 	'.replace('$id', classCategory.id).replace('$name', classCategory.name);

				 	$.each(classCategory.classes, function(key, classes) {
				 		html += '<div class="panel panel-default">\
									<div class="panel-heading">\
										<h4 class="panel-title">\
											<a data-toggle="collapse" data-parent="#accordion-parent-$classCategoryId" href="#accordion-$classId">$name</a>\
										</h4>\
									</div>\
									<div id="accordion-$classId" class="panel-collapse collapse">\
										<div class="panel-body" style="font-size: 15px !important">\
											$description\
											<div class="text-right" $style>\
												<a href="/online-booking/?class=$classId" class="btn btn-white btn-small">BOOK NOW</a>\
											</div>\
										</div>\
									</div>\
								</div>'
						.replace('$classCategoryId', classCategory.id)
						.replace(/\$classId/g, classes.id)
						.replace('$name', classes.name)
						.replace('$style', $.trim(classes.schedules.replace(/\n/g, '')) ? '' : 'style="display:none"')
						.replace('$description', classes.description.replace(/\n/g, '<br/>'));
				 	});

				 	html += '</div>';
				});

				$('.classes-wrapper').html(html);
				$('[data-toggle="collapse"]').collapse({ parent: true, toggle: true });
			}
		},

		PLUGINS: function() {

		},

		EVENTS: function() {

		},

		REQUESTS: {
			getClassCategories: function(defaultValue) {
				$.ajax({
					url: GLOBAL.api.url+'/class-categories/get',
					type: 'GET',
					headers: { 'Authorization': GLOBAL.api.key },
					success: function(data) {
						Classes.METHODS.fixDisplay(data);
					}
				});
			}
		}
	};

	Classes.INIT();
});