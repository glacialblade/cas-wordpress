<?php namespace App\Ajax;

class BookingFormAjax extends \App\Ajax\Core\Ajax {

	// action is used in ajax call -> data: { action: 'sample' }
	// callback is the method to be called like the method sample() below
	// nopriv if this ajax is also accessible by none login users.
	protected $callbacks = [
		['action' => 'get-class-categories', 'callback' => 'getClassCategories', 'nopriv' => true],
		['action' => 'save-booking', 'callback' => 'saveBooking', 'nopriv' => true],
	];

	public function saveBooking() {
		$data = $_POST;
		// $data['account-information']['first_name']        = $data['personal-information']['first_name'];
		// $data['account-information']['last_name']         = $data['personal-information']['last_name'];
		// $data['personal-information']['languages_spoken'] = implode(',', $data['personal-information']['languages_spoken']);

		// $user = User::store($data['account-information']);
		// if($user) {
		// 	$student = Student::save(array_merge($data['personal-information'], $data['employment-status']));
		// }
		// // Email already taken.
		// else {

		// }

		echo json_encode($_POST);
		wp_die();
	}

	public function getClassCategories() {
		// global $wpdb;
		// $classCategories = $wpdb->get_results('SELECT * FROM class_categories WHERE deleted_at IS NULL ORDER BY `order` ASC', OBJECT);

		// foreach($classCategories as &$classCategory) {
		// 	$classCategory->classes = $wpdb->get_results("SELECT * FROM classes WHERE class_category_id = $classCategory->id AND deleted_at IS NULL ORDER BY `order` ASC", OBJECT);
		// 	$classCategory->classes = $classCategory->classes ? $classCategory->classes : [];
		// }

		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL            => 'http://cas-laravel.local/api/class-categories/get',
			CURLOPT_HTTPHEADER     => 'Authorization: OAuth O0ztpWdnyKqC5I8'
		]);

		$resp = json_decode(curl_exec($curl));
		curl_close($curl);

		echo json_encode($resp);
		wp_die();
	}

}