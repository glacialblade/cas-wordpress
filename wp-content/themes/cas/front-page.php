<?php get_header(); ?>
	<main>
		<div style="margin-top: -15px;">
			<?php echo do_shortcode('[pjc_slideshow slide_type="banner"]'); ?>
		</div>
		<div class="brushbg1 container-fluid padTB140 padLR40" style="margin-top: -40px;">
			<div style="display: table; width: 100%; height: 100%;" class="text-center">
				<div style="display: table-cell; vertical-inline: middle;">
					<h2><?= get_post_meta(get_the_ID(), 'first_title', true); ?></h2>
					<p class="fontS34">
						<?= get_post_meta(get_the_ID(), 'first_content', true); ?>
					</p>
				</div>
			</div>
		</div>
		<div class="container-fluid homeSection">
			<div class="row">
				<div class="col-md-8">
					<img class="img-responsive hideIn991" src="<?= get_post_meta(get_the_ID(), 'second_background_image', true); ?>" alt="">
				</div>
				<div class="col-md-4 brushbg2 vertical-center right0 mobileTransWhiteBg">
					<h2><?= get_post_meta(get_the_ID(), 'second_title', true); ?></h2>
					<p><?= get_post_meta(get_the_ID(), 'second_content', true); ?></p>
				</div>
			</div>
		</div>
		<div class="container-fluid bgBlack homeSection">
			<div class="row">
				<div class="what-we-do col-md-4 brushbg3 vertical-center left0 marL40 mobileTransBlackBg">
					<h2 class="text-white"><?= get_post_meta(get_the_ID(), 'third_title', true); ?></h2>
					<p class="text-white"><?= get_post_meta(get_the_ID(), 'third_content', true); ?></p>
				</div>
				<div class="col-md-8 pull-right">
					<img class="img-responsive hideIn991" src="<?= get_post_meta(get_the_ID(), 'third_background_image', true); ?>" alt="">
				</div>
			</div>
		</div>
		<div class="brushbg1 container-fluid padTB140">
			<div class="text-center">
				<h2><?= get_post_meta(get_the_ID(), 'fourth_title', true); ?></h2>
				<p class="fontS34">
					<?= get_post_meta(get_the_ID(), 'fourth_content', true); ?>
				</p>
			</div>
		</div>
		<?php require_once('parts/widgets/gallery.php'); ?>
		<?php require_once('parts/widgets/newsletter.php'); ?>
	</main>
<?php get_footer(); ?>