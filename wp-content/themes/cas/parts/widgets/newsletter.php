<div class="container-fluid">
	<div class="subscribe padTB60">
		<div class="row mobileTransWhiteBg">
			<div class="col-sm-6">
				<h5>
					<span class="light">STAY CONNECTED!</span><br>
					<strong>SUBSCRIBE TO OUR NEWSLETTER</strong>
				</h5>
			</div>
			<div class="col-sm-6 text-right">
				<?= do_shortcode('[mc4wp_form id="123"]'); ?>
			</div>
		</div>
	</div>
</div>