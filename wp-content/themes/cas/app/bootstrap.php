<?php

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

require __DIR__.'/helpers.php';

/**
 * Boot INIT
 */
new \App\App();

/**
 * Boot POST TYPES
 */
$path = __DIR__.'/PostTypes/';
foreach(glob("$path*.php") as $file) {
	// Get the filename -> {$Filename}.php
	$fileExplode = explode('/', $file);
	$className = explode('.', $fileExplode[count($fileExplode)-1])[0];

	// Initialize it automatically.
	$postType = '\App\PostTypes\\'.$className;
	new $postType();
}

/**
 * Boot SHORTCODES
 */
$path = __DIR__.'/Shortcodes/';
foreach(glob("$path*.php") as $file) {
	// Get the filename -> {$Filename}.php
	$fileExplode = explode('/', $file);
	$className = explode('.', $fileExplode[count($fileExplode)-1])[0];

	// Initialize it automatically.
	$postType = '\App\Shortcodes\\'.$className;
	new $postType();
}

/**
 * Boot WIDGETS
 */
$path = __DIR__.'/Widgets/';
foreach(glob($path.'*') as $dir) {
	foreach(glob($dir) as $file) {
		$className = basename($file);

		if($className != 'Core') {
			register_widget('\App\Widgets\\'.$className.'\\'.$className);
		}
	}
}

/**
 * Boot Tiny MCE BUTTON
 */
$path = __DIR__.'/Tinymce/TinymceButton/';
foreach(glob($path.'*') as $dir) {
	foreach(glob($dir) as $file) {
		$className = basename($file);

		if($className != 'Core') {
			$tinymceButton = '\App\Tinymce\\TinymceButton\\'.$className.'\\'.$className;
			new $tinymceButton();
		}
	}
}

/**
 * Boot AJAX
 */
$path = __DIR__.'/Ajax/';
foreach(glob("$path*.php") as $file) {
	// Get the filename -> {$Filename}.php
	$fileExplode = explode('/', $file);
	$className = explode('.', $fileExplode[count($fileExplode)-1])[0];

	// Initialize it automatically.
	$postType = '\App\Ajax\\'.$className;
	new $postType();
}

// $curl = curl_init();

// curl_setopt_array($curl, [
// 	CURLOPT_RETURNTRANSFER => 1,
// 	CURLOPT_URL            => 'http://cas-laravel.local/api/class-categories/get',
// 	CURLOPT_HTTPHEADER     => ['Authorization' => 'OAuth O0ztpWdnyKqC5I8', 'Content-Type' => 'application/json']
// ]);

// $resp = json_decode(curl_exec($curl));
// curl_close($curl);
// var_dump($resp);
// die();

// $curl = curl_init();

// curl_setopt_array($curl, [
// 	CURLOPT_RETURNTRANSFER => 1,
// 	CURLOPT_URL            => "http://cas-laravel.local/api/class-categories/store",
// 	CURLOPT_SSL_VERIFYPEER => false,
// 	CURLOPT_SSL_VERIFYHOST => false,
// 	CURLOPT_POST           => true,
// 	CURLOPT_POSTFIELDS     => json_encode([
// 		'Locale' => 'en_US'
// 	]),
// 	CURLOPT_HTTPHEADER     => [ 'Content-Type: application/json' ]
// ]);

// $resp = json_decode(curl_exec($curl));
// curl_close($curl);
// var_dump($resp);
// die();