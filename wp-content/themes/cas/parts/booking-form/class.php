<p><strong>Choose a Class:</strong></p>
<div class="row" style="margin-top: -20px;">
	<div class="col-md-12 marT20">
		<select name="class_category_id" data-name="class_category_id" disabled class="input input-small">
			<option value="">-- Select a Class Category --</option>
		</select>
	</div>
	<div class="col-md-12 marT20">
		<select name="class_id" data-name="class_id" disabled class="input input-small">
			<option value="">-- Select a Class --</option>
		</select>
	</div>
	<div class="col-md-12 marT20">
		<select name="schedule" data-name="schedule" disabled class="input input-small">
			<option value="">-- Choose your Schedule --</option>
		</select>
	</div>
	<input type="hidden" name="default_class" value="<?= isset($_GET['class']) ? $_GET['class'] : 0 ?>">
</div>
<br>
<button type="submit" class="btn btn-small btn-black">Next Step</button>