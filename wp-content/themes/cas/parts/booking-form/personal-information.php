<p><strong>Personal Information</strong></p>
<div class="personal-information" style="margin-top: -20px;">
	<div class="row">
		<div class="col-md-4 marT20">
			<input type="text" name="last_name" data-name="last_name" id="" class="input input-small" placeholder="Last">
		</div>
		<div class="col-md-4 marT20">
			<input type="text" name="first_name" data-name="first_name" id="" class="input input-small" placeholder="First">
		</div>
		<div class="col-md-4 marT20">
			<input type="text" name="middle_name" data-name="middle_name" id="" class="input input-small" placeholder="Middle">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 marT20">
			<input type="text" name="birth_date" data-name="birth_date" id="" class="input input-small date" placeholder="Birth Date">
		</div>
		<div class="col-md-4 marT20">
			<select name="gender" data-name="gender" class="input input-small">
				<option value="">-- Gender --</option>
				<option value="Male">Male</option>
				<option value="Female">Female</option>
			</select>
		</div>
		<div class="col-md-4 marT20">
			<select name="citizenship" data-name="citizenship" class="input input-small">
				<option value="">-- Citizenship --</option>
				<option value="Filipino">Filipino</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 marT20">
			<select name="languages_spoken" data-name="languages_spoken" class="input input-small multiselect" multiple="multiple">
				<option value="Tagalog">Tagalog</option>
				<option value="English">English</option>
				<option value="Hangeul">Hangeul</option>
				<option value="Nihonggo">Nihonggo</option>
			</select>
		</div>
		<div class="col-md-4 marT20">
			<select name="educational_background" data-name="educational_background" class="input input-small">
				<option value="">-- Educational Background --</option>
				<option value="abc">Abc</option>
			</select>
		</div>
		<div class="col-md-4 marT20">
			<select name="classification" data-name="classification" class="input input-small">
				<option value="">-- Classification --</option>
				<option value="Test">Test</option>
			</select>
		</div>
	</div>
</div>