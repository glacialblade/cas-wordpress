$(document).ready(function() {

	var ContactForm = {

		el: {
			form: $('[name=contact-form]')
		},

		INIT: function() {
			this.PLUGINS();
			this.EVENTS();
		},

		PLUGINS: function() {
			this.el.form.validate({
				rules: { name: 'required', phone: 'required', email: { email: true, required: true }, message: 'required' },
				messages: { name: '', phone: '', email: '', message: '' }
			});
		},

		EVENTS: function() {

			/**
			 * On Form Submit
			 */
			this.el.form.submit(function(e) {
				var form = $(this);
				e.preventDefault();

				if(form.valid()) {
			    	ContactForm.REQUESTS.send(form);
				}
			});
		},

		REQUESTS: {
			send: function(form) {
				var data = form.serializeObject();
				data.action = 'contact-form';

				GLOBAL.showLoader(form, true);
				form.find('.alert').hide();
				$.ajax({
			        url:      ajaxurl,
			        type:     'POST',
			        dataType: 'json',
			        data:      data,
			        success: function(data) {
			        	GLOBAL.showLoader(form, false);
			        	form.find('input, textarea').val('');
			        	form.find('.alert-success').fadeIn();
			        },
			        error: function() {
			        	form.find('.alert-danger').fadeIn();
			        }
			    });
			}
		}

	};

	ContactForm.INIT();
	
});