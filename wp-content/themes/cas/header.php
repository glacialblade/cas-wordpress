<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?= get_bloginfo('name'); ?> - <?= get_bloginfo('description'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript">var ajaxurl = "<?= admin_url('admin-ajax.php'); ?>";</script>
		<?php wp_head(); ?>
		<style>
			.frs-slideshow-container .frs-wrapper .frs-caption .frs-caption-content p {
				font-family: Nexa Light;
				line-height: 40px !important;
			}
			.frs-caption strong, .frs-caption b {
				font-family: Nexa Bold;
			}
			.frs-slideshow-container .frs-wrapper.frs-responsive-mobile-small .frs-caption,
			.frs-slideshow-container .frs-wrapper.frs-responsive-mobile-medium .frs-caption {
				
			}
			.frs-slideshow-container .frs-wrapper.frs-responsive-mobile-small .frs-caption .frs-caption-content p {
				font-size: 25px !important;
				line-height: 35px !important;
			}

			.frs-slideshow-container .frs-wrapper.frs-responsive-mobile-small .frs-caption .frs-caption-content p.frs-caption-button {
				
			}
		</style>
	
		<link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/assets/images/favicon.png">
		<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/bower_components/bootstrap/dist/css/bootstrap.css">
		<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/bower_components/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
		<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css">
		<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/css/main.css">
	</head>
	
	<body <?php body_class(); ?>>
		<div class="container-fluid">
			<header>
				<div class="header">
					<div class="row">
						<div class="col-sm-4">
							<a href="/"><img class="img-responsive logo" src="<?= get_template_directory_uri(); ?>/assets/images/logo.png" alt="CAS"></a>
						</div>
						<div class="col-sm-8 text-right hideInTablet">
							<nav>
								<?php wp_nav_menu([
									'theme_location' => 'header',
									'menu_class' => 'navigation'
								]); ?>
							</nav>
							<div class="social-icons hideInTablet">
								<?php $socialMedias = new WP_Query([ 'post_type' => 'social-media', 'post_status' => 'publish', 'posts_per_page' => 5, 'order_by' => 'date', 'order' => 'DESC']); ?>
								<ul>
									<?php while($socialMedias->have_posts()): $socialMedias->the_post(); ?>
										<li><a target="_blank" href="<?= get_post_meta(get_the_ID(), 'url', true); ?>"><span class="fa <?= get_post_meta(get_the_ID(), 'icon', true); ?>" aria-hidden="true"></span></a></li>
									<?php endwhile; wp_reset_postdata(); ?>
								</ul>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="trigger-menu"><span class="fa fa-reorder"></span></div>
						</div>
					</div>
					<div class="row">
						<div class="responsive-nav">
							<div class="responsive-menu">
								<?php while($socialMedias->have_posts()): $socialMedias->the_post(); ?>
									<div class="mobile-social"><a target="_blank" style="color: #000;" href="<?= get_post_meta(get_the_ID(), 'url', true); ?>"><span class="fa <?= get_post_meta(get_the_ID(), 'icon', true); ?>" aria-hidden="true"></span></a></div>
								<?php endwhile; wp_reset_postdata(); ?>
								<?php wp_nav_menu([
									'theme_location' => 'header',
									'menu_class' => 'responsive-nav-list'
								]); ?>
							</div>
						</div>
					</div>
				</div>
			</header>
		</div>
