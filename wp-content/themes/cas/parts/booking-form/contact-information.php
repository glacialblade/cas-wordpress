<p><strong>Address & Telephone Number</strong><br>Permanent Home Address</p>
<div class="contact-information" style="margin-top: -20px;">
	<div class="row">
		<div class="col-md-4 marT20">
			<input type="text" name="street_number" data-name="street_number" id="" class="input input-small" placeholder="Number and Street">
		</div>
		<div class="col-md-4 marT20">
			<input type="text" name="barangay" data-name="barangay" id="" class="input input-small" placeholder="Subd / Village, Brgy.">
		</div>
		<div class="col-md-4 marT20">
			<input type="text" name="city_town_province" data-name="city_town_province" id="" class="input input-small" placeholder="City, Town, Province">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 marT20">
			<input type="text" name="postal_code" data-name="postal_code" id="" class="input input-small" placeholder="Postal Code">
		</div>
		<div class="col-md-4 marT20">
			<input type="text" name="office_number" data-name="office_number" id="" class="input input-small" placeholder="Office Number">
		</div>
		<div class="col-md-4 marT20">
			<input type="text" name="home_number" data-name="home_number" id="" class="input input-small" placeholder="Home Number">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 marT20">
			<input type="text" name="mobile_number" data-name="mobile_number" id="" class="input input-small" placeholder="Mobile Number">
		</div>
	</div>
</div>