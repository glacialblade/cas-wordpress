<?php namespace App\PostTypes;

class PagePostType extends \App\PostTypes\Core\PostType {
	
	protected $metabox = [

        'First Strip' => [
            'id'         => 'first_strip',
            'title'      => 'First Strip',
            'pages'      => ['page'],
            'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ),
            'show_names' => true,
            'fields'     => [
                [ 'name' => 'Title', 'id' => 'first_title', 'type' => 'text' ],
                [ 'name' => 'Content', 'id' => 'first_content', 'type' => 'wysiwyg', 'options' => ['textarea_rows' => 5] ]
            ]
        ],

        'Second Strip' => [
            'id'         => 'second_strip',
            'title'      => 'Second Strip',
            'pages'      => ['page'],
            'show_names' => true,
            'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ),
            'fields'     => [
                [ 'name' => 'Title', 'id' => 'second_title', 'type' => 'text' ],
                [ 'name' => 'Content', 'id' => 'second_content', 'type' => 'wysiwyg', 'options' => ['textarea_rows' => 5] ],
                [ 'name' => 'Background Image', 'id' => 'second_background_image', 'type' => 'file', 'description' => 'Size: 1200 x 938' ],
            ]
        ],

        'Third Strip' => [
            'id'         => 'third_strip',
            'title'      => 'Third Strip',
            'pages'      => ['page'],
            'show_names' => true,
            'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ),
            'fields'     => [
                [ 'name' => 'Title', 'id' => 'third_title', 'type' => 'text' ],
                [ 'name' => 'Content', 'id' => 'third_content', 'type' => 'wysiwyg', 'options' => ['textarea_rows' => 5] ],
                [ 'name' => 'Background Image', 'id' => 'third_background_image', 'type' => 'file', 'description' => 'Size: 1300 x 938' ],
            ]
        ],

        'Fourth Strip' => [
            'id'         => 'fourth_strip',
            'title'      => 'Fourth Strip',
            'pages'      => ['page'],
            'show_names' => true,
            'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ),
            'fields'     => [
                [ 'name' => 'Title', 'id' => 'fourth_title', 'type' => 'text' ],
                [ 'name' => 'Content', 'id' => 'fourth_content', 'type' => 'wysiwyg', 'options' => ['textarea_rows' => 5] ],
                // [ 'name' => 'Background Image', 'id' => 'second_strip_background_image', 'type' => 'file' ],
            ]
        ],

        'Contact Us Fields' => [
            'id'         => 'contact_us_fields',
            'title'      => 'Contact Us Fields',
            'pages'      => ['page'],
            'show_names' => true,
            'show_on'    => array( 'key' => 'id', 'value' => array( 16, ), ),
            'fields'     => [
                [ 'name' => 'Left Content', 'id' => 'left_content', 'type' => 'wysiwyg', 'options' => ['textarea_rows' => 8] ],
                [ 'name' => 'Right Content', 'id' => 'right_content', 'type' => 'wysiwyg', 'options' => ['textarea_rows' => 8] ],
            ]
        ],
    ];

	// Enqueue scripts here.
	public function scripts() {

	}

	// Custom Logic Here
	public function custom() {

	}

}