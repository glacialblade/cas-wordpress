<p><strong>Work Background from Present to Past</strong></p>
<?php foreach(['First', 'Second', 'Third'] as $key=>$label): ?>
<p><?= $label; ?></p>
<div class="work-background" style="margin-top:-20px;">
	<div class="row">
		<div class="col-md-4 marT20">
			<input type="text" name="date_employed_<?= $key; ?>" data-name="date_employed" class="input input-small date" placeholder="Date Employed">
		</div>
		<div class="col-md-4 marT20">
			<input type="text" name="occupation_<?= $key; ?>" data-name="occupation" class="input input-small" placeholder="Occupation">
		</div>
		<div class="col-md-4 marT20">
			<input type="text" name="name_of_employer_<?= $key; ?>" data-name="name_of_employer" class="input input-small" placeholder="Name of Employer">
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 marT20">
			<input type="text" name="address_<?= $key; ?>" data-name="address" class="input input-small" placeholder="Complete Address of Employer">
		</div>
		<div class="col-md-6 marT20">
			<input type="text" name="salary_<?= $key; ?>" data-name="salary" class="input input-small" placeholder="Monthly Income / Salary">
		</div>
	</div>
</div>
<br>
<?php endforeach; ?>