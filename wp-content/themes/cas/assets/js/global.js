GLOBAL = {
    api: {
        url: (location.hostname ==  'cas.com.ph' || location.hostname ==  'staging.cas.com.ph') ? 'http://administrator.cas.com.ph/api' : 'http://cas-laravel.local/api',
        key: 'O0ztpWdnyKqC5I8'
    },

    showLoader: function($form, show) {
        $form.find('input, textarea, select, checkbox, button').removeAttr('disabled');

        if(show) {
            $form.find('input, textarea, select, checkbox, button').attr('disabled', 'disabled');    
        }
    },

    simpleDataParser: function($wrapper) {
        var wrapper = [];
        var name    = false;

        $.each($wrapper, function(key, val) {
            wrapper[key] = {};

            $.each($(this).find('input[type=text], input[type=email],input, input[type=password], input[type=number], input[type=hidden], select, textarea, checkbox, radio'), function() {
                name = $(this).attr('data-name');

                if(name) {
                    wrapper[key][name] = $(this).val();
                }
            });
        })

        if(wrapper.length == 1) {
            wrapper = wrapper[0];
        }

        return wrapper;
    }
}

$(document).ready(function() {
    $(".responsive-nav").hide();

    $(".trigger-menu").click(function(){
        $(".responsive-nav").slideToggle();
    });

    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        startView: 'years'
    });

    $('.multiselect').multiselect();

    function updateNewsletterButton() {
        if($(window).width() <= 767) {
            $(".subscribeSubmit").html("Submit")
        } else {
            $(".subscribeSubmit").html('<span class="fa fa-arrow-right text-white"></span>');
        }
    }
    $(window).resize(function() {
        updateNewsletterButton()
    });
    updateNewsletterButton();
});
