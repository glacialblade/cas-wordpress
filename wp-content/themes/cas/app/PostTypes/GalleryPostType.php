<?php namespace App\PostTypes;

class GalleryPostType extends \App\PostTypes\Core\PostType {

	// If the post type you want to control is page you don't need to specify these Attributes Anymore.

	// Post Type Name
	protected $label    = 'Gallery';
	protected $type     = 'gallery';

	// Default Not Required.
	protected $icon     = 'dashicons-images-alt2';
	protected $taxonomy = false;
	protected $exclude  = false;
	protected $supports = ['title', 'thumbnail'];
	
	// Enqueue scripts here.
	public function scripts() {

	}

	// Custom Logic Here
	public function custom() {

	}

}