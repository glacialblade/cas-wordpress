<p>
<strong>Please login if you already have an account.</strong>
<br/>
<small><strong><em>If you forget your password you may <a href="javascript:void(0)" id="reset-password" style="color: black; font-family: Nexa Light; text-decoration: underline; font-size: 15px;"><strong>reset your password here.</strong></a></em></strong></small>
</p>
<div class="alert alert-danger" style="display: none;">
	Invalid Email or Password.
</div>
<div class="row" style="margin-top: -20px;">
	<div class="col-md-6 marT20">
		<input type="text" name="email" data-name="email" class="input input-small" placeholder="Email Address">
	</div>
	<div class="col-md-6 marT20">
		<input type="password" name="password" data-name="password" class="input input-small" placeholder="Password">
	</div>
	<div class="col-md-12 marT20">
		<button type="button" class="btn btn-small btn-black change-tab" data-show="class" class="btn btn-small btn-black">Back</button>&nbsp;
		<button type="submit" class="btn btn-small btn-black">Use my Account Details</button>&nbsp;
		<button type="button" data-show="booking" class="btn btn-small btn-black change-tab">Continue Registration</button>&nbsp;
	</div>
</div>