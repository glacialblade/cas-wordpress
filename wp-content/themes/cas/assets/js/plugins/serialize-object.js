/**
 *  Serialize Object Plugin
 */

(function ($) {
    $.fn.serialize = function (options) {
        return this.serializeArray(options);
    };

    $.fn.serializeArray = function (options) {
        var o = $.extend({
            checkboxesAsNumber: false
        }, options || {});

        var rselectTextarea = /select|textarea/i;
        var rinput = /text|hidden|password|email|number|search|color/i;

        return this.map(function () {
            return this.elements ? $.makeArray(this.elements) : this;
        })
            .filter(function () {
                return this.name && !this.disabled &&
                    (this.checked
                    || (o.checkboxesAsNumber && this.type === 'checkbox')
                    || rselectTextarea.test(this.nodeName)
                    || rinput.test(this.type));
            })
            .map(function (i, elem) {
                var val = $(this).val();
                return val == null ?
                    null :
                    $.isArray(val) ?
                        $.map(val, function (val, i) {
                            return { name: elem.name, value: val };
                        }) :
                    {
                        name: elem.name,
                        value: (o.checkboxesAsNumber && this.type === 'checkbox') ? //moar ternaries!
                            (this.checked ? 1 : 0) :
                            val
                    };
            }).get();
    };
})(jQuery);

(function($) {
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serialize({ checkboxesAsNumber: true });

        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value);
            } else {
                o[this.name] = this.value;
            }
        });

        return o;
    };
})( jQuery );