<div class="container-fluid">
	<div class="gallery-shots">
		<?php $galleries = new WP_Query([ 'post_type' => 'gallery', 'post_status' => 'publish', 'posts_per_page' => -1, 'order_by' => 'date', 'order' => 'ASC' ]); ?>
		<div class="row">
			<?php while($galleries->have_posts()): $galleries->the_post(); ?>
				<div class="col-xs-4"><img class="img-responsive" src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" alt=""></div>
			<?php endwhile; ?>
		</div>
	</div>
</div>