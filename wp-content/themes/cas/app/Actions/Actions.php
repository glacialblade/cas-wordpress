<?php namespace App\Actions;

/**
 * Class Actions
 * Put all your functions here to be used on templates.
 *
 * @package App\Actions
 */
class Actions {

	public function sampleFunction() {
		echo 'Hello World';
	}

}