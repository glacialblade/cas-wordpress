<?php get_header(); ?>
	
	<main>
		<div class="pagetitle" style="background: url(<?php the_post_thumbnail_url( 'full' ) ?>) center center no-repeat;">
			<h1>News</h1>
		</div>
		<div class="inner-content">
			<div class="container-fluid padLR140">
				<div class="col-sm-8">
					<?php while(have_posts()): the_post(); ?>
						<h2><a class="text-black fontS25" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<p class="fontS20">by <?php the_author(); ?> // <?php the_time('F j, Y'); ?></p>
						<div class="featured-image">
							<?php the_post_thumbnail('large', ['class' => 'img-responsive']); ?>
						</div>
						<br>
						<p class="fontS20"><?php the_content() ?></p>
					<?php endwhile; ?>
				</div>
				<div class="sidebar col-sm-4">
					<?php require_once('parts/widgets/featured-stories.php'); ?>
				</div>
			</div>
		</div>
		<?php require_once('parts/widgets/newsletter.php'); ?>
	</main>
<?php get_footer(); ?>