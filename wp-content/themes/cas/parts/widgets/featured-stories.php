<h3 class="titleBlack fontS30">FEATURED STORIES</h3>

<?php $featuredPosts = new WP_Query([ 'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 5, 'order_by' => 'date', 'order' => 'DESC', 'meta_query' => ['key' => 'featured', 'value' => 'Yes']]); ?>

<?php while($featuredPosts->have_posts()): $featuredPosts->the_post(); ?>
	<div class="featstory">
		<p class="padTB20">
			<img class="img-responsive" width="100%" src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" alt="">
		</p>
		<p class="uppercase"><a class="text-black fontS25" href="<?php the_permalink(); ?>"><?= get_the_title(); ?></a></p>
		<p class="fontS20">by <?php the_author(); ?> // <?php the_time('F j, Y'); ?></p>
		<p class="fontS20"><?= get_post_meta(get_the_ID(), 'list_content', true); ?></p>
	</div>
<?php endwhile; wp_reset_postdata(); ?>