<?php
/*
Template Name: Booking Form
*/

get_header(); ?>

<main id="booking-page">
	<div class="pagetitle" style="background: url(<?php the_post_thumbnail_url( 'full' ) ?>) center center no-repeat;">
		<h1><?php the_title(); ?></h1>
	</div>
	<div class="inner-content brushbg1">
		<div class="container-fluid padLR140 two-column">
			<div class="row">
				<ul class="booking-wizard-menu" style="margin-bottom: 40px;">
					<li id="booking-wizard-item-class" class="current">Choose a Class & Term</li>
					<li id="booking-wizard-item-login">Login</li>
					<li id="booking-wizard-item-booking">Fill Up Form</li>
					<li id="booking-wizard-item-thankyou">Finish Booking</li>
				</ul>
				<!-- SELECT CLASS !-->
				<div id="tab-class">
					<h3 class="text-center">Select a Class</h3>
					<div class="row">
						<div class="col-md-4">
							<form name="class">
								<?php require_once(__DIR__.'/../parts/booking-form/class.php'); ?>
							</form>
						</div>
					</div>
				</div>
				
				<div id="tab-login" style="display: none">
					<form name="login">
						<?php require_once(__DIR__.'/../parts/booking-form/login.php'); ?>
					</form>
					<form name="reset-password">
						<?php require_once(__DIR__.'/../parts/booking-form/reset-password.php'); ?>
					</form>
				</div>

				<!-- BOOKING FORM !-->
				<div id="tab-booking" style="display: none">
					<form name="booking">
						<h3 class="text-center">Kindly fill-up this form for our records.</h3>
						<div class="alert alert-danger" style="display: none;">The email you've already exists.</div>
						<br><br>
						<?php require_once(__DIR__.'/../parts/booking-form/account-information.php'); ?>
						<br><br>
						<?php require_once(__DIR__.'/../parts/booking-form/personal-information.php'); ?>
						<br><br>
						<?php require_once(__DIR__.'/../parts/booking-form/work-background.php'); ?>
						<br>
						<?php require_once(__DIR__.'/../parts/booking-form/employment-status.php'); ?>
						<br><br>
						<?php require_once(__DIR__.'/../parts/booking-form/contact-information.php'); ?>
						<br><br>
						<?php require_once(__DIR__.'/../parts/booking-form/socio-economic-data.php'); ?>
						<button type="button" class="btn btn-small btn-black change-tab" data-show="login" class="btn btn-small btn-black">Back</button> &nbsp;
						<button type="submit" class="btn btn-small btn-black">Submit Form</button>
					</form>
				</div>

				<!-- THANK YOU !-->
				<div id="tab-thank-you" style="display: none">
					<h3 class="text-center">Thank You</h3>
					<p class="text-center">
						Thank you for booking our representative will get in touch with you.
						<br><br>
						<a href="/" class="btn btn-black"><strong>Go Back Home</strong></a>
					</p>
				</div>
			</div>
		</div>
	</div>
	</div>
</main>

<?php get_footer(); ?>