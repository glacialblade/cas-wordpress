<?php namespace App\Shortcodes;

class ClassesShortcode extends \App\Shortcodes\Core\Shortcode {

	protected $shortcode = 'classes';

	public function generate($atts) {
		$html = '';
		global $wpdb;
		$classCategories = $wpdb->get_results('SELECT * FROM class_categories WHERE deleted_at IS NULL ORDER BY `order` ASC', OBJECT);

		foreach($classCategories as $classCategory) {
			$accordions = '';
			$classes = $wpdb->get_results("SELECT * FROM classes WHERE class_category_id = $classCategory->id AND deleted_at IS NULL ORDER BY `order` ASC", OBJECT);
			
			foreach($classes as $class) {
				$accordions .= "<div class='panel panel-default'>
									<div class='panel-heading'>
										<h4 class='panel-title'>
											<a data-toggle='collapse' data-parent='#accordion-parent-$classCategory->id' href='#accordion-$class->id'>$class->name</a>
										</h4>
									</div>
									<div id='accordion-$class->id' class='panel-collapse collapse'>
										<div class='panel-body smaller'>
											$class->description
											<div class='text-right'>
												<button class='btn btn-white'>BOOK NOW</button>
											</div>
										</div>
									</div>
								</div>
			";
			}

			$html .= "
				<h2 class='fontS50 text-center'>$classCategory->name</h2>
				<div id='accordion-parent-$classCategory->id' class='accordion panel-group'>
					$accordions
				</div>
			";
				// <h2 class='fontS50 text-center'>$classCategory->name</h2>
				// <ul class='classes-table'>
				// 	$li
				// </ul>
		}

		return $html;
	}

}