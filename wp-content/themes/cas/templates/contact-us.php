<?php
/*
Template Name: Contact Us
*/

get_header(); ?>
	
	<main>
		<div class="pagetitle" style="background: url(<?php the_post_thumbnail_url( 'full' ) ?>) center center no-repeat;">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="inner-content brushbg1">
			<div class="container-fluid padLR140 two-column">
				<div class="row">
					<?php while(have_posts()): the_post(); ?>
						<div class="col-sm-12">
							<?php the_content(); ?>
							<br><br>
						</div>
						<div class="col-sm-4 left-content">
							<?= wpautop(get_post_meta(get_the_ID(), 'left_content', true)); ?>
							<p><strong>Connect with us!</strong></p>
							<?php $socialMedias = new WP_Query([ 'post_type' => 'social-media', 'post_status' => 'publish', 'posts_per_page' => 5, 'order_by' => 'date', 'order' => 'DESC']); ?>
							<?php while($socialMedias->have_posts()): $socialMedias->the_post(); ?>
								<a style="color: black;" target="_blank" href="<?= get_post_meta(get_the_ID(), 'url', true); ?>"><span class="fa <?= get_post_meta(get_the_ID(), 'icon', true); ?>" aria-hidden="true"></span></a> &nbsp; &nbsp; 
							<?php endwhile; wp_reset_postdata(); ?>
						</div>
						<div class="col-sm-8 right-content">
							<?= do_shortcode(wpautop(get_post_meta(get_the_ID(), 'right_content', true))); ?>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
		</div>
	</main>
<?php get_footer(); ?>