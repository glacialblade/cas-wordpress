<p><strong>Socio-Economic Data</strong><br>Furnish all the required information on each family member listed. Check DECEASED after the name of the deceased family members. Write down mother's maiden name. Under the column "Highest Educational Attainment" indicate the educational level which the household member actually completed. (e.g. Grade III, Third Year, High School Graduate, Second Year College, B.S.E or Ph.D)</p>
<br>
<?php foreach(['Father', 'Mother', 'Legal Guardian', 'Spouse'] as $key=>$member): ?>
	<p><?= $member; ?></p>
	<div class="socio-economic-data" style="margin-top: -20px;">
		<div class="row">
		    <div class="col-md-4 marT20">
				<input type="text" name="socio_name_<?= $key; ?>" data-name="name" class="input input-small" placeholder="Name">
		    </div>
		    <div class="col-md-4 marT20">
				<input type="text" name="socio_citizenship_<?= $key; ?>" data-name="citizenship" class="input input-small" placeholder="Citizenship">
		    </div>
		    <div class="col-md-4 marT20">
				<input type="text" name="socio_status_<?= $key; ?>" data-name="status" class="input input-small" placeholder="Status">
		    </div>
	    </div>
		<div class="row">
		    <div class="col-md-6 marT20">
				<input type="text" name="socio_educational_attainment_<?= $key; ?>" data-name="educational_attainment" class="input input-small" placeholder="Educational Attainment">
		    </div>
		    <div class="col-md-6 marT20">
				<input type="text" name="socio_occupation_<?= $key; ?>" data-name="occupation" class="input input-small" placeholder="Occupation">
				<input type="hidden" name="type" data-name="type" value="<?= $member; ?>">
		    </div>
		</div>
	</div>
	<br>
<?php endforeach; ?>