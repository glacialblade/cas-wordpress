<?php get_header(); ?>
	
	<main>
		<div class="pagetitle" style="background: url(<?= get_the_post_thumbnail_url(14, 'full'); ?>) center center no-repeat;">
			<h1>News</h1>
		</div>
		<div class="inner-content">
			<div class="container-fluid padLR140">
				<div class="col-sm-8">
					<?php while(have_posts()): the_post(); ?>
						<div class="featured-image">
							<?php the_post_thumbnail('full', ['class' => 'img-responsive']); ?>
						</div>
						<p class="padT20 uppercase"><a class="text-black fontS25" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
						<p class="fontS20">by <?php the_author(); ?> // <?php the_date(); ?></p>
						<p class="fontS20"><?= get_post_meta(get_the_ID(), 'list_content', true); ?></p>
						<br/><br/><br/>
					<?php endwhile; ?>
				</div>
				<div class="sidebar col-sm-4">
					<?php require_once('parts/widgets/featured-stories.php'); ?>
				</div>
			</div>
		</div>
		<?php require_once('parts/widgets/newsletter.php'); ?>
	</main>
<?php get_footer(); ?>