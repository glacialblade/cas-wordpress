<?php namespace App\PostTypes;

class PostPostType extends \App\PostTypes\Core\PostType {
	
	protected $metabox = [

        'Post Fields' => [
            'id'         => '',
            'title'      => 'Post Fields',
            'pages'      => ['post'],
            'show_names' => true,
            'fields'     => [
                [ 'name' => 'List Content', 'id' => 'list_content', 'type' => 'textarea_small' ],
                [ 'name' => 'Featured', 'id' => 'featured', 'type' => 'select', 'options' => ['No' => 'No', 'Yes' => 'Yes'] ],
            ]
        ]

    ];

	// Enqueue scripts here.
	public function scripts() {

	}

	// Custom Logic Here
	public function custom() {

	}

}