<div class="account-information">
	<p>
		<strong>Account Information</strong>
		<small class="password-note text-info"><br/><em><strong>Leave the password blank if you don't want to change it.</strong></em></small>
	</p>
	<div class="row" style="margin-top: -20px">
		<div class="col-md-4 marT20">
			<input type="text" name="email" data-name="email" class="input input-small" placeholder="Email Address">
		</div>
		<div class="col-md-4 marT20">
			<input type="password" name="password" data-name="password" class="input input-small" placeholder="Password">
		</div>
		<div class="col-md-4 marT20">
			<input type="password" name="confirm_password" id="" class="input input-small" placeholder="Confirm Password">
			<input type="hidden" name="student_id" data-name="student_id" />
		</div>
	</div>
</div>